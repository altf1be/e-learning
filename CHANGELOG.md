# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.3](https://bitbucket.org/altf1be/e-learning/compare/v0.0.2...v0.0.3) (2019-09-09)



### [0.0.2](https://bitbucket.org/altf1be/e-learning/compare/v0.0.1...v0.0.2) (2019-09-06)



### 0.0.1 (2019-09-06)



## [1.1.8](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.7...v1.1.8) (2019-05-21)



## [1.1.7](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.6...v1.1.7) (2019-05-21)



## [1.1.6](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.5...v1.1.6) (2019-05-03)



## [1.1.5](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.4...v1.1.5) (2019-05-03)



## [1.1.4](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.3...v1.1.4) (2019-05-03)



## [1.1.3](https://bitbucket.org/altf1be/software-architecture/compare/v0.0.0...v1.1.3) (2019-05-03)



## [1.1.2](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.1...v1.1.2) (2019-04-22)



## [1.1.1](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.0...v1.1.1) (2019-04-22)



# [1.1.0](https://bitbucket.org/altf1be/software-architecture/compare/v0.0.1...v1.1.0) (2019-04-22)


### Bug Fixes

* **chore:** update the version number to match highest value ([444e930](https://bitbucket.org/altf1be/software-architecture/commits/444e930))



## [0.0.1](https://bitbucket.org/altf1be/software-architecture/compare/v1.0.1...v0.0.1) (2019-04-22)


### Bug Fixes

* **chore:** rename buildall.bat to keep a kind of naming convention ([7522ef6](https://bitbucket.org/altf1be/software-architecture/commits/7522ef6))



# [0.0.0](https://bitbucket.org/altf1be/software-architecture/compare/v1.0.1...v0.0.0) (2019-05-02)

# [](https://bitbucket.org/altf1be/software-architecture/compare/v1.0.1...v) (2019-04-21)

# [](https://bitbucket.org/altf1be/software-architecture/compare/v1.0.0...v) (2019-04-18)

#  (2019-04-18)

### Bug Fixes

* **docs:** corrent the format of the NGROK documentation ([aab8497](https://bitbucket.org/altf1be/software-architecture/commits/aab8497))
* Could not import extension sphinxcontrib.gist on rtfd.org ([4e61a4a](https://bitbucket.org/altf1be/software-architecture/commits/4e61a4a))
* **chore:** adapt the variables to display the name of the project including spaces in the PDF ([35731a0](https://bitbucket.org/altf1be/software-architecture/commits/35731a0))
* **chore:** delete build\latexpdf when build.pdf.bat is launched ([ca994a5](https://bitbucket.org/altf1be/software-architecture/commits/ca994a5))
* **chore:** rename image to match the required case sensitiveness ([6eacccd](https://bitbucket.org/altf1be/software-architecture/commits/6eacccd))
* **chore:** rename image to match the required case sensitiveness ([66d8f99](https://bitbucket.org/altf1be/software-architecture/commits/66d8f99))
* **chore:** rename images, indicate the size of the images ([7ad34da](https://bitbucket.org/altf1be/software-architecture/commits/7ad34da))
* **docs:** add a backup strategy ([6881377](https://bitbucket.org/altf1be/software-architecture/commits/6881377))
* **docs:** add missing spaces for bullet points ([f97bab5](https://bitbucket.org/altf1be/software-architecture/commits/f97bab5))
* **docs:** correct the heading 1 ([0123490](https://bitbucket.org/altf1be/software-architecture/commits/0123490))
* **docs:** improve the quality of the titles + add picture of ci-cd by microsoft ([7e2ec66](https://bitbucket.org/altf1be/software-architecture/commits/7e2ec66))
* **docs:** reduce the scale of the images to be displayed correctly on a PDF ([333edce](https://bitbucket.org/altf1be/software-architecture/commits/333edce))
* **gist:** rtfd.org crashes when sphinxcontrib.gist is enabled, replaced the ..gist by ..raw :: html ([64c780c](https://bitbucket.org/altf1be/software-architecture/commits/64c780c))
* remove spaces of the project name because it crashes the generation of the pdf ([a10766d](https://bitbucket.org/altf1be/software-architecture/commits/a10766d))
* removed the version of the requirement ([ecc73fb](https://bitbucket.org/altf1be/software-architecture/commits/ecc73fb))

### Features

* add a 'badge' indicating if the build of the documentation succeeded on https://www.rtfd.org ([09bbda4](https://bitbucket.org/altf1be/software-architecture/commits/09bbda4))
* add ALT-F1-Application_deployment.rst ALT-F1-Bug_template.rst ALT-F1-Curriculum_DotNet.rst ALT-F1-GDPR.rst ALT-F1-Research_and_development.rst ALT-F1-Sales_and_Marketing.rst ALT-F1-Technologies.rst ([bfaf451](https://bitbucket.org/altf1be/software-architecture/commits/bfaf451))
