.. the header contains shared information by documentation pages

.. URL Links

.. _Angular: https://angular.io/
.. _Apache HTTP server: _http://httpd.apache.org/
.. _NGINX: https://www.nginx.com/
.. _nodejs: https://nodejs.org/en/
.. _PHP: https://www.php.net/
.. _SIREMIS: https://siremis.asipto.com
.. _AETA: https://www.aeta-audio.com/en/
.. _JITSI: https://github.com/jitsi/libjitsi

.. Substitutions of Images

.. |stratex_data_model.png| image:: _static/img/stratex_data_model-700x525.png
			:alt: StratEx Data model
			:width: 700 px
			:height: 525 px
			:scale: 100 %

.. |gitmodel.png| image:: _static/img/git-model_2x-700x927.png
			:alt: By Vincent Driessen on Tuesday, January 05, 2010
			:width: 700 px
			:height: 927 px
			:scale: 100 %
	
.. |baremetrics.io-dashboard.png| image:: _static/img/baremetrics.io-dashboard-700x408.png
			:alt: financial dashboard - credits: baremetrics.io
			:width: 700 px
			:height: 408 px
			:scale: 100 %
			
.. |tfs.ci-cd-process.png| image:: _static/img/visualstudio.microsoft.com/AnyDeveloperAnyLanguage_636x350-op.png
			:alt: CI/CD process by Team Foundation Service - credits: microsoft.com
			:width: 636 px
			:height: 350 px
			:scale: 100 %

.. |Michael_Porters_Value_Chain.svg| image:: _static/img/wikipedia.com/Michael_Porters_Value_Chain-700x369.png
			:alt: Michael Porter's Value Chain. Denis Fadeev [CC BY-SA 3.0 (https://creativecommons.org/licenses/by-sa/3.0)]
			:width: 700 px
			:height: 369 px
			:scale: 100 %

.. |cicd-CIvsCIvsCD.png| image:: _static/img/atlassian.com/cicd-CIvsCIvsCD-700x393.png
			:alt: Continous Integration vs. Continous Delivery vs. Continuous Development - credits : atlassian.com
			:width: 700 px
			:height: 393 px
			:scale: 100 %
			
.. |choosing-a-good-chart-09-1-2000x1700.png| image:: _static/pdf/extremepresentation.typepad.com/choosing-a-good-chart-09-1-2000x1700.png
			:alt: Chart Suggestions - A Thought-Starter - credits : (c) A. Abela - a.v.abela@gmail.com
			:width: 1700 px
			:height: 2000 px
			:scale: 50 %