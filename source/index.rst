.. include:: ___header.rst

SIP Secure by BY-Systems
==============================

Contact us : https://by-systems.weebly.com


.. todolist::


.. toctree::
   :maxdepth: 3
   :numbered:
   :caption: Contents
   
   00-00.Secure_and_Private_AI.rst
   glossary

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
